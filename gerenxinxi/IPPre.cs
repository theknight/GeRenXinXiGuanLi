﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
//using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace gerenxinxi
{
    public partial class IPPre : Form
    {
        public IPPre()
        {
            InitializeComponent();
        }
        public String IP = "";
        private void button1_Click(object sender, EventArgs e)
        {
            String textip= textBox1.Text.ToString().Trim();
            if (IsIPAddress(textip))
            {
                IP = textip;
                Console.WriteLine("输入："+IP);
                this.Visible = false;
                this.Close();
            }
            else {
                MessageBox.Show("请输入正确的IP地址");
            }
        }
        public static bool IsIPAddress(string ip)
        {

            if (string.IsNullOrEmpty(ip) || ip.Length < 7 || ip.Length > 15) return false;

            string regformat = @"^(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])\.(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])\.(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])\.(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])$";

            Regex regex = new Regex(regformat, RegexOptions.IgnoreCase);

            return regex.IsMatch(ip);

        }



        public static bool IsIPPort(string port)
        {

            bool isPort = false;

            int portNum;

            isPort = Int32.TryParse(port, out portNum);

            if (isPort && portNum >= 0 && portNum <= 65535)
            {

                isPort = true;

            }

            else
            {

                isPort = false;

            }

            return isPort;
        }
    
    
    
    }
}
