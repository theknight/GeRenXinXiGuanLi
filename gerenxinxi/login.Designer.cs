﻿namespace gerenxinxi
{
    partial class frm_loading
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_loading));
            this.enter = new System.Windows.Forms.Button();
            this.exit1 = new System.Windows.Forms.Button();
            this.username = new System.Windows.Forms.TextBox();
            this.password = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // enter
            // 
            this.enter.BackColor = System.Drawing.Color.Transparent;
            this.enter.BackgroundImage = global::gerenxinxi.Properties.Resources.l2;
            this.enter.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.enter.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.enter.ForeColor = System.Drawing.Color.White;
            this.enter.Location = new System.Drawing.Point(109, 225);
            this.enter.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.enter.Name = "enter";
            this.enter.Size = new System.Drawing.Size(105, 36);
            this.enter.TabIndex = 0;
            this.enter.UseVisualStyleBackColor = false;
            this.enter.Click += new System.EventHandler(this.enter_Click);
            // 
            // exit1
            // 
            this.exit1.BackColor = System.Drawing.Color.Transparent;
            this.exit1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("exit1.BackgroundImage")));
            this.exit1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.exit1.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.exit1.Location = new System.Drawing.Point(372, 225);
            this.exit1.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.exit1.Name = "exit1";
            this.exit1.Size = new System.Drawing.Size(101, 36);
            this.exit1.TabIndex = 1;
            this.exit1.UseVisualStyleBackColor = false;
            this.exit1.Click += new System.EventHandler(this.exit1_Click);
            // 
            // username
            // 
            this.username.BackColor = System.Drawing.Color.White;
            this.username.Location = new System.Drawing.Point(277, 124);
            this.username.Margin = new System.Windows.Forms.Padding(4, 3, 4, 2);
            this.username.Name = "username";
            this.username.Size = new System.Drawing.Size(153, 23);
            this.username.TabIndex = 4;
            // 
            // password
            // 
            this.password.BackColor = System.Drawing.Color.White;
            this.password.Location = new System.Drawing.Point(277, 160);
            this.password.Margin = new System.Windows.Forms.Padding(4, 2, 4, 3);
            this.password.Name = "password";
            this.password.PasswordChar = '*';
            this.password.Size = new System.Drawing.Size(153, 23);
            this.password.TabIndex = 5;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label1.ForeColor = System.Drawing.SystemColors.InactiveCaption;
            this.label1.Location = new System.Drawing.Point(161, 198);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(0, 14);
            this.label1.TabIndex = 6;
            this.label1.Visible = false;
            // 
            // frm_loading
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.BackgroundImage = global::gerenxinxi.Properties.Resources.lbg;
            this.ClientSize = new System.Drawing.Size(571, 285);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.password);
            this.Controls.Add(this.username);
            this.Controls.Add(this.exit1);
            this.Controls.Add(this.enter);
            this.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.Name = "frm_loading";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "系统登陆";
            this.Load += new System.EventHandler(this.frm_loading_Load);
            this.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.frm_loading_MouseDoubleClick);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button enter;
        private System.Windows.Forms.Button exit1;
        private System.Windows.Forms.TextBox username;
        private System.Windows.Forms.TextBox password;
        private System.Windows.Forms.Label label1;

    }
}

