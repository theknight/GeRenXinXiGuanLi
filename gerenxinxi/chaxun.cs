﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
//using System.Linq;
using System.Text;
using System.Windows.Forms;
//using System.Web.Script.Serialization;
using Newtonsoft.Json;
using System.IO;
//using System.Runtime.Serialization.Json;
using System.Runtime.Serialization;

namespace gerenxinxi
{
    public partial class chaxun : Form
    {
        public chaxun()
        {
            InitializeComponent();
        }
        public Boolean fla = false;
        public String username;
        public String password;
        public String ip;
        public String userid;
        public int canjia;
        private void 考试查询_Load(object sender, EventArgs e)
        {
            String address = "http://"+ip+"/onlinetest/getPaper";
            Dictionary<String, String> dmap = new Dictionary<string, string>();
            dmap.Add("username", username);
            dmap.Add("userpass", password);
            dmap.Add("fla", "paper");
            String dataStream = "";
            canjia = 0;
            try
            {
                dataStream = Utils.HttpPost(address, dmap);
                String decstring = Base64.DecodeBase64(dataStream);
                JsonReader readerjs = new JsonTextReader(new StringReader(decstring));
                String pro = null;
                String pere = null;
                Dictionary<String, String> dictionary = new Dictionary<String, String>();//创建集合
                int a = 0;
                while (readerjs.Read())
                {
                    if (readerjs.TokenType.ToString().Equals("PropertyName"))
                    {
                        pro = readerjs.Value.ToString();
                    }
                    if (readerjs.TokenType.ToString().Equals("String"))
                    {
                        pere = readerjs.Value.ToString();
                    }
                    if (pro != null && pere != null)
                    {
                        dictionary.Add(pro, pere);
                        Console.WriteLine(pro + " " + pere);
                        if (dictionary.Count > 11)
                        {
                            String id = "";
                            String st = "";
                            String et = "";
                            String ct = "";
                            String cou = "";
                            String pn = "";
                            dictionary.TryGetValue("PAPER_NAME", out pn);
                            dictionary.TryGetValue("ID", out id);
                            dictionary.TryGetValue("STARTTIME", out st);
                            dictionary.TryGetValue("ENDTIME", out et);
                            dictionary.TryGetValue("PAPER_MINUTE", out ct);
                            dictionary.TryGetValue("TOTAL_SCORE", out cou);
                            listView1.Items.Add(id);
                            listView1.Items[a].SubItems.Add(pn);
                            listView1.Items[a].SubItems.Add(st);
                            listView1.Items[a].SubItems.Add(et);
                            listView1.Items[a].SubItems.Add(ct+"分钟");
                            listView1.Items[a].SubItems.Add(cou);
                            a++;
                            dictionary.Clear();
                        }
                        pro = null;
                        pere = null;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("网络故障或服务器异常。");
                Console.WriteLine(ex);
                return;
            }
          
        }

        private void 考试查询_FormClosed(object sender, FormClosedEventArgs e)
        {
            fla = true;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (listView1.SelectedItems.Count > 0)
            {
                DialogResult dr= MessageBox.Show("考试过程中请勿退出客户端","提示", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
                if (dr == DialogResult.OK)
                {
                    //点确定的代码
                    String paperid = listView1.SelectedItems[0].SubItems[0].Text;
                    Console.WriteLine(listView1.SelectedItems[0].SubItems[0].Text);
                    String request = "http://" + ip + "/onlinetest/getPaper?fla=" + Base64.EncodeBase64("ks") + "&u=" + Base64.EncodeBase64(userid) + "&p=" + Base64.EncodeBase64(paperid);

                    //打开指定的网页
                    System.Diagnostics.Process.Start(request);
                    canjia = 1;
                    this.Close();
                    ////启动钩子，处理钩子事件
                    //cadstop.Hook_Start();
                    //Console.WriteLine("启动钩子");
                    ////屏蔽任务管理器
                    //cadstop.TaskMgrLocking(true);
                    //Console.WriteLine("屏蔽任务管理器");
                }
                else
                {        //点取消的代码        
                }
               
            }
            else {
                MessageBox.Show("请选择考试项。");
            }
        }
    }
    //Paper公用类
    class Paper
    {
        public string POSTDATE { set; get; }
        public string PAPER_NAME { set; get; }
        public string QORDER { set; get; }
        public string PAPER_MINUTE { set; get; }
        public string ID { set; get; }
        public string REMARK { set; get; }
        public string ADMINID { set; get; }
        public string ENDTIME { set; get; }
        public string STARTTIME { set; get; }
        public string STATUS { set; get; }
        public string SHOW_SCORE { set; get; }
        public string TOTAL_SCORE { set; get; }
    }
}