﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
//using System.Linq;
using System.Net;
using System.Text;

namespace gerenxinxi
{
    class Utils
    {
        public static Dictionary<String, String> getFromJson(String decstring)
        {
            Dictionary<String, String> dictionary = new Dictionary<String, String>();//创建集合
            JsonReader readerjs = new JsonTextReader(new StringReader(decstring));
            String pro = null;
            String pere = null;
            while (readerjs.Read())
            {
                    if (readerjs.TokenType.ToString().Equals("PropertyName"))
                    {
                        pro = readerjs.Value.ToString();
                    }
                    if (readerjs.TokenType.ToString().Equals("String"))
                    {
                        pere = readerjs.Value.ToString();
                    }
                    if (pro != null && pere != null)
                    {
                        dictionary.Add(pro, pere);
                        pro = null;
                        pere = null;
                    }
            }
            return dictionary;
        }

        /// <summary>
        /// address 目标地址
        /// peremes post的参数
        /// 默认对发送的数据的值进行base64编码
        /// </summary>
        /// <returns></returns>
        public static String HttpPost(String address,Dictionary<String,String> peremes)  {
            // Create a request using a URL that can receive a post. 
            WebRequest request = WebRequest.Create(address);
            // Set the Method property of the request to POST.
            request.Method = "POST";
            // Create POST data and convert it to a byte array.
            String st = "";
           foreach(KeyValuePair<String,String> tem in peremes){
               st =st + tem.Key.ToString() + "=" + Base64.EncodeBase64(tem.Value.ToString())+"&";
           }
           string postData = st;
           Console.WriteLine(st);
            //postData = Base64.EncodeBase64(postData);
            byte[] byteArray = Encoding.UTF8.GetBytes(postData);
            // Set the ContentType property of the WebRequest.
            request.ContentType = "application/x-www-form-urlencoded";
            // Set the ContentLength property of the WebRequest.
            request.ContentLength = byteArray.Length;
            // Get the request stream.
            Stream dataStream = request.GetRequestStream();
            // Write the data to the request stream.
            dataStream.Write(byteArray, 0, byteArray.Length);
            // Close the Stream object.
            dataStream.Close();
            // Get the response.
            //在这里对服务器返回异常进行处理
            WebResponse response = request.GetResponse();
            // Display the status.
            Console.WriteLine(((HttpWebResponse)response).StatusDescription);
            // Get the stream containing content returned by the server.
            dataStream = response.GetResponseStream();
            // Read the content.
            StreamReader reader = new StreamReader(dataStream);
            // Display the content.
            string responseFromServer = reader.ReadToEnd();
            dataStream.Close();
            response.Close();
            return responseFromServer;
        }

    }
}
